# Singleton SD - DOCKER Chromium XVFB

Contains necessary files to create a docker image with chromium browser with and without nodejs.

## IMAGES

* **singletonsd/docker/chromium-xvfb/base:** contains only chromium.
* **singletonsd/docker/chromium-xvfb/nodejs-9X:** contains nodejs version 9.8.0-1.
* **singletonsd/docker/chromium-xvfb/nodejs-8X:** contains nodejs version 8.10.0-1.

----------------------

© [Singleton](http://singletonsd.com), Italy, 2019.
